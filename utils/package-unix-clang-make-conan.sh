#!/usr/bin/env sh
# -*- coding: utf-8 -*-

# Creates a build environment adapted for package making on Unix-like system,
# based on Clang and Make, using Conan.

# Clang C command.
CC=$(which clang)
# Clang C++ command.
CXX=$(which clang++)

conan install .. -s compiler=clang  -s compiler.version=5.0 -s build_type=Debug --build=missing -if ../build

cmake .. -DCMAKE_CXX_COMPILER=$CXX -DCMAKE_BUILD_TYPE=Release \
    -DCMAKE_CXX_FLAGS_DEBUG="-g -pedantic -Wall -Werror -pipe" \
    -DCMAKE_CXX_FLAGS_MINSIZEREL="-march=native -Os -DNDEBUG -pipe" \
    -DCMAKE_CXX_FLAGS_RELEASE="-march=native -O3 -DNDEBUG -pipe" \
    -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="-march=native -O2 -g -pipe -fno-omit-frame-pointer" \
    -DMAN=TRUE -G "Unix Makefiles" -B ../build -DUSE_CONAN=TRUE
