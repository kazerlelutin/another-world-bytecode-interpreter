#!/usr/bin/env sh
# -*- coding: utf-8 -*-

# Creates the build environment for Clang and Ninja, using Conan, on a Unix-like
# system.

# Clang C command.
CC=$(which clang)
# Clang C++ command.
CXX=$(which clang++)

conan install .. -s compiler=clang  -s compiler.version=5.0 -s build_type=Debug --build=missing -if ../build

cmake .. -DCMAKE_CXX_COMPILER=$CXX -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_CXX_FLAGS_DEBUG="-g -pedantic -Wall -Werror -pipe" \
    -DCMAKE_CXX_FLAGS_MINSIZEREL="-march=native -Os -DNDEBUG -pipe" \
    -DCMAKE_CXX_FLAGS_RELEASE="-march=native -O3 -DNDEBUG -pipe" \
    -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="-march=native -O2 -g -pipe -fno-omit-frame-pointer" \
    -DHTML=TRUE -G "Ninja" -DFORCE_COLORED_OUTPUT=TRUE -B ../build -DUSE_CONAN=TRUE
