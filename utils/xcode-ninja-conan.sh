#!/usr/bin/env sh
# -*- coding: utf-8 -*-

# Creates the build environment for Xcode using Ninja compilation tools and
# Conan.

conan install .. -if ../build --build=missing

cmake .. -DCMAKE_BUILD_TYPE=Debug \
    -DCMAKE_CXX_FLAGS_DEBUG="-g -pedantic -Wall -Werror -pipe" \
    -DCMAKE_CXX_FLAGS_MINSIZEREL="-march=native -Os -DNDEBUG -pipe" \
    -DCMAKE_CXX_FLAGS_RELEASE="-march=native -O3 -DNDEBUG -pipe" \
    -DCMAKE_CXX_FLAGS_RELWITHDEBINFO="-march=native -O2 -g -pipe -fno-omit-frame-pointer" \
    -DDOCSET=TRUE -G "Ninja" -B ../build -DUSE_CONAN=TRUE
