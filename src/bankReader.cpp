/**
 * \file bankReader.cpp
 * \brief Accessing data banks for the game.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Glaize, Sylvain
 * \author willll
 */

#include "bankReader.hpp"

#include <cassert>
#include <memory>

namespace AnotherWorld {
    namespace Private {
        /**
         * \brief Identifies the name of the bank to be open.
         * \param me Memory describer.
         * \returns Name of the file.
         */
        std::string getBankNameFromEntry (const MemEntry* me) {
            boost::format fmt("%02x");
            return "bank" +
                   boost::str(fmt % static_cast<std::uint32_t>(me->bankId));
        }

        /**
         * \brief Opens a file containing assets.
         * \param file File describer.
         * \param me Memory describer.
         * \param directory Directory where the file is located.
         * \returns Whether or not the file is open.
         */
        bool openBankFile (File &file, const MemEntry* me,
                           const std::string &directory) {
            /// Name of the current bank.
            const auto bankName = getBankNameFromEntry(me);
            if (!openFile(file, bankName, directory, me->fileEndianness)) {
                Logger::getLogger().error("Bank::read() unable to open '%s'",
                                          bankName.c_str());
                return false;
            }

            return true;
        }

    }

    BankReader::BankReader (const std::string &directory, const MemEntry* me,
                            std::uint8_t *buffer) {
        /// Name of the bank to be read.
        AnotherWorld::File bankFile;
        if (!Private::openBankFile(bankFile, me, directory)) {
            bankIsValid = false;
            return;
        }

        bankFile.seek(me->bankOffset);
        /// Endianness the files comply with.
        endianness = bankFile.endianness();

        bankFile.read(reinterpret_cast<char *>(buffer), me->packedSize);

        if (me->isPacked()) {
            // The input and output pointers start at the end of the buffer
            // and go backward toward the start of the buffer
            bufferStart = buffer;
            inputPointer = buffer + me->packedSize - 4;
            bankIsValid = unpack();
        }
    }

    bool BankReader::unpack () {
        unpackContext.dataSize = readLongFromInput();
        outputPointer = bufferStart + unpackContext.dataSize - 1;
        unpackContext.crc = readLongFromInput();
        unpackContext.currentWord = readLongFromInput();
        unpackContext.crc ^= unpackContext.currentWord;

        do {
            if (!getNextBit()) {
                if (!getNextBit()) {
                    /// Size of the entry.
                    const auto lengthSize = 3;
                    /// How many additional bytes in the entry.
                    const auto additionalBytes = 1;
                    copyNextBytes(lengthSize, additionalBytes);
                } else {
                    /// Size of the entry.
                    const auto length = 2;
                    /// Data offset in memory.
                    const auto distanceSize = 8;
                    copyPreviousSequence(distanceSize, length);
                }
            } else {
                /// Type of data to be treated.
                std::uint16_t value = getBits(2);
                assert(value < 4);
                switch (value) {
                    case 0:
                    case 1: {
                        /// Data offset in memory.
                        const auto distanceSize = value + 9;
                        /// Data length.
                        const auto length = value + 3;
                        copyPreviousSequence(distanceSize, length);
                        break;
                    }
                    case 2: {
                        /// Data offset in memory.
                        const auto distanceSize = 12;
                        /// Data length.
                        const auto length = getBits(8) + 1;
                        copyPreviousSequence(distanceSize, length);
                        break;
                    }
                    case 3: {
                        /// Data length.
                        const auto lengthSize = 8;
                        /// How many additional bytes in the entry.
                        const auto additionalBytes = 9;
                        copyNextBytes(lengthSize, additionalBytes);
                        break;
                    }
                    default:
                        assert(false && "Invalid case");
                }
            }
        } while (unpackContext.dataSize > 0);

        return unpackContext.crc == 0;
    }

    void BankReader::copyNextBytes (std::uint8_t lengthSize,
                                    std::uint8_t addCount) {
        /// How many bytes to access.
        std::uint16_t byteCount = getBits(lengthSize) + addCount;

        Logger::getLogger().debug(ILogger::DBG_BANK,
                                  "Bank::copyNextBytes (%d, %d) byte count=%d",
                                  lengthSize, addCount, byteCount);

        assert(byteCount <= unpackContext.dataSize);
        unpackContext.dataSize -= byteCount;

        while (byteCount--) {
            assert(outputPointer >= inputPointer &&
                   outputPointer >= bufferStart);
            *outputPointer = static_cast<uint8_t>(getBits(8));
            --outputPointer;
        }
    }

    void BankReader::copyPreviousSequence (std::uint8_t distanceSize,
                                           std::uint16_t length) {
        /// Data offset in memory.
        const std::uint16_t distance = getBits(distanceSize);
        Logger::getLogger().debug(
            ILogger::DBG_BANK, "Bank::copyPreviousSequence(%d, %d) distance=%d",
            distanceSize, length, distance
        );
        unpackContext.dataSize -= length;

        /// How many bytes left to read.
        std::uint16_t count = length;
        while (count--) {
            assert(outputPointer >= inputPointer &&
                   outputPointer >= bufferStart);
            *outputPointer = *(outputPointer + distance);
            --outputPointer;
        }
    }

    std::uint32_t BankReader::readLongFromInput () {
        /// Value after correcting endianness.
        const auto result = file2Native(
                *reinterpret_cast<std::uint32_t *>(inputPointer), endianness
        );
        inputPointer -= 4;
        return result;
    }

    std::uint16_t BankReader::getBits (std::uint8_t bitCount) {
        /// Bits accumulator.
        std::uint16_t accumulator = 0;
        while (bitCount--) {
            accumulator <<= 1;
            if (getNextBit()) {
                accumulator |= 1;
            }
        }
        return accumulator;
    }

    bool BankReader::getNextBit () {
        /**
         * Gets the next bit and reads a new word if the current one
         * was exhausted.
         */
        bool carryFlag = getChunkBitAndShiftRight();
        if (unpackContext.currentWord == 0) {
            assert(inputPointer >= bufferStart);
            unpackContext.currentWord = readLongFromInput();
            unpackContext.crc ^= unpackContext.currentWord;
            carryFlag = getChunkBitAndShiftRight();
            unpackContext.currentWord |= 0x80000000;
        }
        return carryFlag;
    }

    bool BankReader::getChunkBitAndShiftRight () {
        /// Gets the next bit from the current word.
        const bool carryFlag = (unpackContext.currentWord & 1);
        unpackContext.currentWord >>= 1;
        return carryFlag;
    }
}
