/**
 * \file vm.cpp
 * \brief Implementation of the game virtual machine.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Glaize, Sylvain
 */

#include "vm.hpp"

#include <algorithm>
#include <cassert>
#include <cstring>
#include <ctime>

#include "mixer.hpp"
#include "parts.hpp"
#include "resources.hpp"
#include "sfxplayer.hpp"
#include "sys.hpp"
#include "video.hpp"
#include "log.hpp"

namespace Private {
    bool isLegacy(AnotherWorld::DataType dataType) {
        return dataType == AnotherWorld::DT_ATARI ||
               dataType == AnotherWorld::DT_AMIGA;
    }
}

const AnotherWorld::VirtualMachine::OpcodeStub
        AnotherWorld::VirtualMachine::opcodeTable [] = {
    /* 0x00 */
    &VirtualMachine::op_movConst, &VirtualMachine::op_mov,
    &VirtualMachine::op_add, &VirtualMachine::op_addConst,
    /* 0x04 */
    &VirtualMachine::op_call, &VirtualMachine::op_ret,
    &VirtualMachine::op_pauseThread, &VirtualMachine::op_jmp,
    /* 0x08 */
    &VirtualMachine::op_setSetVect, &VirtualMachine::op_jnz,
    &VirtualMachine::op_condJmp, &VirtualMachine::op_setPalette,
    /* 0x0C */
    &VirtualMachine::op_resetThread, &VirtualMachine::op_selectVideoPage,
    &VirtualMachine::op_fillVideoPage, &VirtualMachine::op_copyVideoPage,
    /* 0x10 */
    &VirtualMachine::op_blitFramebuffer, &VirtualMachine::op_killThread,
    &VirtualMachine::op_drawString, &VirtualMachine::op_sub,
    /* 0x14 */
    &VirtualMachine::op_and, &VirtualMachine::op_or, &VirtualMachine::op_shl,
    &VirtualMachine::op_shr,
    /* 0x18 */
    &VirtualMachine::op_playSound, &VirtualMachine::op_updateMemList,
    &VirtualMachine::op_playMusic};

AnotherWorld::VirtualMachine::VirtualMachine (Mixer* mix, Resource* _res,
                                              SfxPlayer* ply, Video* vid,
                                              SDLStub* stub,
                                              AnotherWorld::DataType dataType):
        mixer(mix), res(_res), player(ply), video(vid), sys(stub), scriptPtr(),
        stackPtr(0), gotoNextThread(false), lastTimeStamp(0) {
    memset(vmVariables, 0, sizeof(vmVariables));
    vmVariables[0x54] = 0x81;
    vmVariables[VM_VARIABLE_RANDOM_SEED] = static_cast<std::uint16_t>(time(0));
#ifdef BYPASS_PROTECTION
    // These 3 registers are set by the game code.
    vmVariables[0xBC] = 0x10;
    vmVariables[0xC6] = 0x80;
    vmVariables[0xF2] = Private::isLegacy(dataType) ? 6000 : 4000;
    // This register is set by the engine executable.
    vmVariables[0xDC] = 33;
#endif

    player->setMarkVar(&vmVariables[VM_VARIABLE_MUS_MARK]);

    lastTimeStamp = 0;
}

void AnotherWorld::VirtualMachine::op_movConst () {
    /// Identifier of the register to be handled.
    const std::uint8_t variableId = scriptPtr.fetchByte();
    /// Value of the constant.
    const std::int16_t value = scriptPtr.fetchWord(sys->fileEndian());
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_movConst(0x%02X, %d)",
                              variableId, value);
    vmVariables[variableId] = value;
}

void AnotherWorld::VirtualMachine::op_mov () {
    /// Register who will receive the value.
    const std::uint8_t dstVariableId = scriptPtr.fetchByte();
    /// Register from which get the value.
    const std::uint8_t srcVariableId = scriptPtr.fetchByte();
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_mov(0x%02X, 0x%02X)",
                              dstVariableId, srcVariableId);
    vmVariables[dstVariableId] = vmVariables[srcVariableId];
}

void AnotherWorld::VirtualMachine::op_add () {
    /// Register to which a value will be added.
    const std::uint8_t dstVariableId = scriptPtr.fetchByte();
    /// Register which value will be added to the other.
    const std::uint8_t srcVariableId = scriptPtr.fetchByte();
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_add(0x%02X, 0x%02X)",
                              dstVariableId, srcVariableId);
    vmVariables[dstVariableId] += vmVariables[srcVariableId];
}

void AnotherWorld::VirtualMachine::op_addConst () {
    if (res->currentPartId() == 0x3E86 &&
        scriptPtr.pc == res->segBytecode() + 0x6D48) {
        Logger::getLogger().warning(
            "VirtualMachine::op_addConst() hack for non-stop looping gun sound "
            "bug");
        /* The script 0x27 slot 0x17 doesn't stop the gun sound from looping, I
         * don't really know why; for now, let's play the 'stopping sound' like
         * the other scripts do
         * (0x6D43) jmp(0x6CE5)
         * (0x6D46) break
         * (0x6D47) VAR(6) += -50 */
        snd_playSound(0x5B, 1, 64, 1);
    }
    /// Register to which the value will be added.
    const std::uint8_t variableId = scriptPtr.fetchByte();
    /// Value to add.
    const std::int16_t value = scriptPtr.fetchWord(sys->fileEndian());
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_addConst(0x%02X, %d)",
                              variableId, value);
    vmVariables[variableId] += value;
}

void AnotherWorld::VirtualMachine::op_call () {
    /// Position of the value in the stack.
    const std::uint16_t offset = scriptPtr.fetchWord(sys->fileEndian());
    /// Pointer to access the stack of the virtual machine.
    const std::uint8_t sp = stackPtr;

    Logger::getLogger().debug(ILogger::DBG_VM, "VirtualMachine::op_call(0x%X)",
                              offset);
    scriptStackCalls[sp] =
        static_cast<std::uint16_t>(scriptPtr.pc - res->segBytecode());
    if (stackPtr == 0xFF) {
        Logger::getLogger().error(
            "VirtualMachine::op_call() ec=0x%X stack overflow", 0x8F);
    }
    ++stackPtr;
    scriptPtr.pc = res->segBytecode() + offset;
}

void AnotherWorld::VirtualMachine::op_ret () {
    Logger::getLogger().debug(ILogger::DBG_VM, "VirtualMachine::op_ret()");
    if (stackPtr == 0) {
        Logger::getLogger().error("VirtualMachine::op_ret() ec=0x%X stack underflow", 0x8F);
    }
    --stackPtr;
    /// Pointer to access the stack of the virtual machine.
    const std::uint8_t sp = stackPtr;
    scriptPtr.pc = res->segBytecode() + scriptStackCalls[sp];
}

void AnotherWorld::VirtualMachine::op_pauseThread () {
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_pauseThread()");
    gotoNextThread = true;
}

void AnotherWorld::VirtualMachine::op_jmp () {
    /// Position where to jump into opcode.
    const std::uint16_t pcOffset = scriptPtr.fetchWord(sys->fileEndian());
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_jmp(0x%02X)", pcOffset);
    scriptPtr.pc = res->segBytecode() + pcOffset;
}

void AnotherWorld::VirtualMachine::op_setSetVect () {
    /// Thread identifier.
    const std::uint8_t threadId = scriptPtr.fetchByte();
    /// Position where to go into opcode.
    const std::uint16_t pcOffsetRequested =
        scriptPtr.fetchWord(sys->fileEndian());
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_setSetVect(0x%X, 0x%X)",
                              threadId, pcOffsetRequested);
    threadsData[REQUESTED_PC_OFFSET][threadId] = pcOffsetRequested;
}

void AnotherWorld::VirtualMachine::op_jnz () {
    /// Identifier of the register to be tested.
    const std::uint8_t i = scriptPtr.fetchByte();
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_jnz(0x%02X)", i);
    --vmVariables[i];
    if (vmVariables[i] != 0) {
        op_jmp();
    } else {
        scriptPtr.fetchWord(sys->fileEndian());
    }
}

void AnotherWorld::VirtualMachine::op_condJmp () {
    /// Opcode identifier.
    const std::uint8_t opcode = scriptPtr.fetchByte();
    /// Identifier of the register in which the tested value resides.
    const std::uint8_t var = scriptPtr.fetchByte();
    /// Value for the test.
    const std::int16_t b = vmVariables[var];
    /// Value to be tested.
    std::int16_t a;

    if (opcode & 0x80) {
        a = vmVariables[scriptPtr.fetchByte()];
    } else if (opcode & 0x40) {
        a = scriptPtr.fetchWord(sys->fileEndian());
    } else {
        a = scriptPtr.fetchByte();
    }
    Logger::getLogger().debug(
        ILogger::DBG_VM, "VirtualMachine::op_condJmp(%d, 0x%02X, 0x%02X)",
        opcode, b, a);

    /// Check if the conditional value is met.
    bool expr = false;
    switch (opcode & 7) {
        case 0:  // jz
            expr = (b == a);
#ifdef BYPASS_PROTECTION
            if (res->currentPartId() == 16000) {
                /* 0CB8: jmpIf(VAR(0x29) == VAR(0x1E), @0CD3)
                 * ... */
                if (b == 0x29 && (opcode & 0x80) != 0) {
                    // 4 symbols
                    vmVariables[0x29] = vmVariables[0x1E];
                    vmVariables[0x2A] = vmVariables[0x1F];
                    vmVariables[0x2B] = vmVariables[0x20];
                    vmVariables[0x2C] = vmVariables[0x21];
                    // counters
                    vmVariables[0x32] = 6;
                    vmVariables[0x64] = 20;
                    Logger::getLogger().warning(
                        "Script::op_condJmp() bypassing protection");
                    expr = true;
                }
            }
#endif
            break;
        case 1:  // jnz
            expr = (b != a);
            break;
        case 2:  // jg
            expr = (b > a);
            break;
        case 3:  // jge
            expr = (b >= a);
            break;
        case 4:  // jl
            expr = (b < a);
            break;
        case 5:  // jle
            expr = (b <= a);
            break;
        default:
            Logger::getLogger().warning(
                "VirtualMachine::op_condJmp() invalid condition %d",
                (opcode & 7));
            break;
    }

    if (expr) {
        op_jmp();
    } else {
        scriptPtr.fetchWord(sys->fileEndian());
    }
}

void AnotherWorld::VirtualMachine::op_setPalette () {
    /// Identifier of the wanted palette.
    const std::uint16_t paletteId = scriptPtr.fetchWord(sys->fileEndian());
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_changePalette(%d)",
                              paletteId);
    video->setPaletteIdRequested(paletteId >> 8);
}

void AnotherWorld::VirtualMachine::op_resetThread () {
    /// Thread identifier.
    const std::uint8_t threadId = scriptPtr.fetchByte();
    /// To be completed ...
    std::uint8_t i = scriptPtr.fetchByte();

    // Makes sure i is within [0 ... VM_NUM_THREADS - 1]
    i = i & (VM_NUM_THREADS - 1);
    /// To be completed ...
    std::int8_t n = i - threadId;

    if (n < 0) {
        Logger::getLogger().warning(
            "VirtualMachine::op_resetThread() ec=0x%X (n < 0)", 0x880);
        return;
    }
    ++n;
    /// To be completed ...
    const std::uint8_t a = scriptPtr.fetchByte();

    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_resetThread(%d, %d, %d)",
                              threadId, i, a);

    if (a == 2) {
        /// To be completed ...
        std::uint16_t* p = &threadsData[REQUESTED_PC_OFFSET][threadId];
        while (n--) {
            *p++ = 0xFFFE;
        }
    } else if (a < 2) {
        // To be completed ...
        std::uint8_t* p = &vmIsChannelActive[REQUESTED_STATE][threadId];
        while (n--) {
            *p++ = a;
        }
    }
}

void AnotherWorld::VirtualMachine::op_selectVideoPage () {
    /// Identifier of the frame buffer to be used.
    const std::uint8_t frameBufferId = scriptPtr.fetchByte();
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_selectVideoPage(%d)",
                              frameBufferId);
    video->changePagePtr1(frameBufferId);
}

void AnotherWorld::VirtualMachine::op_fillVideoPage () {
    /// Identifier of the framebuffer to be filled.
    const std::uint8_t pageId = scriptPtr.fetchByte();
    /// Filling colour.
    const std::uint8_t colour = scriptPtr.fetchByte();
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_fillVideoPage(%d, %d)",
                              pageId, colour);
    video->fillPage(pageId, colour);
}

void AnotherWorld::VirtualMachine::op_copyVideoPage () {
    /// Identifier of the framebuffer which will receive data.
    const std::uint8_t srcPageId = scriptPtr.fetchByte();
    /// Identifier of the framebuffer which will send data.
    const std::uint8_t dstPageId = scriptPtr.fetchByte();
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_copyVideoPage(%d, %d)",
                              srcPageId, dstPageId);
    video->copyPage(srcPageId, dstPageId, vmVariables[VM_VARIABLE_SCROLL_Y]);
}

void AnotherWorld::VirtualMachine::op_blitFramebuffer () {
    /// Identifier of the framebuffer to be treated.
    const std::uint8_t pageId = scriptPtr.fetchByte();
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_blitFramebuffer(%d)", pageId);
    inp_handleSpecialKeys();

    /// To be completed.
    const std::int32_t delay = sys->getTimeStamp() - lastTimeStamp;
    /// To be completed.
    const std::int32_t timeToSleep =
        vmVariables[VM_VARIABLE_PAUSE_SLICES] * 20 - delay;

    // The bytecode will set vmVariables[VM_VARIABLE_PAUSE_SLICES] from 1 to 5
    // The virtual machine hence indicate how long the image should be
    // displayed.

    if (timeToSleep > 0) {
        sys->sleep(timeToSleep);
    }

    lastTimeStamp = sys->getTimeStamp();

    // WTF ?
    vmVariables[0xF7] = 0;

    video->updateDisplay(pageId);
}

void AnotherWorld::VirtualMachine::op_killThread() {
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_killThread()");
    scriptPtr.pc = res->segBytecode() + 0xFFFF;
    gotoNextThread = true;
}

void AnotherWorld::VirtualMachine::op_drawString() {
    /// Identifier of the string to be written.
    const std::uint16_t stringId = scriptPtr.fetchWord(sys->fileEndian());
    /// Abscissa of the string on screen.
    const std::uint16_t x = scriptPtr.fetchByte();
    /// Ordinate of the string on screen.
    const std::uint16_t y = scriptPtr.fetchByte();
    /// Colour of the string on screen.
    const std::uint8_t colour = scriptPtr.fetchByte();

    Logger::getLogger().debug(
        ILogger::DBG_VM, "VirtualMachine::op_drawString(0x%03X, %d, %d, %d)",
        stringId, x, y, colour);

    video->drawString(colour, x, y, stringId);
}

void AnotherWorld::VirtualMachine::op_sub () {
    /// Identifier of the register to which subtract the value.
    const std::uint8_t i = scriptPtr.fetchByte();
    /// Identifier of the register from which get the value to subtract.
    const std::uint8_t j = scriptPtr.fetchByte();
    Logger::getLogger().debug(
        ILogger::DBG_VM, "VirtualMachine::op_sub(0x%02X, 0x%02X)", i, j);
    vmVariables[i] -= vmVariables[j];
}

void AnotherWorld::VirtualMachine::op_and () {
    /// Identifier of the register to which apply the logical and.
    const std::uint8_t variableId = scriptPtr.fetchByte();
    /// Value for the logical and.
    const std::uint16_t n = scriptPtr.fetchWord(sys->fileEndian());
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_and(0x%02X, %d)", variableId,
                              n);
    vmVariables[variableId] =
        static_cast<std::uint16_t>(vmVariables[variableId]) & n;
}

void AnotherWorld::VirtualMachine::op_or () {
    /// Identifier of the register to which apply the logical or.
    const std::uint8_t variableId = scriptPtr.fetchByte();
    /// Value for the logical or.
    const std::uint16_t value = scriptPtr.fetchWord(sys->fileEndian());
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_or(0x%02X, %d)", variableId,
                              value);
    vmVariables[variableId] =
        static_cast<std::uint16_t>(vmVariables[variableId]) | value;
}

void AnotherWorld::VirtualMachine::op_shl () {
    /// Identifier of the register to shift.
    const std::uint8_t variableId = scriptPtr.fetchByte();
    /// How many bits should be shift.
    const std::uint16_t leftShiftValue =
        scriptPtr.fetchWord(sys->fileEndian());
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_shl(0x%02X, %d)", variableId,
                              leftShiftValue);
    vmVariables[variableId] =
        static_cast<std::uint16_t>(vmVariables[variableId]) << leftShiftValue;
}

void AnotherWorld::VirtualMachine::op_shr () {
    /// Identifier of the register to shift.
    const std::uint8_t variableId = scriptPtr.fetchByte();
    /// How many bits should be shift.
    const std::uint16_t rightShiftValue =
        scriptPtr.fetchWord(sys->fileEndian());
    Logger::getLogger().debug(
        ILogger::DBG_VM, "VirtualMachine::op_shr(0x%02X, %d)", variableId,
        rightShiftValue);
    vmVariables[variableId] =
        static_cast<uint16_t>(vmVariables[variableId]) >> rightShiftValue;
}

void AnotherWorld::VirtualMachine::op_playSound () {
    /// Identifier of the sound in assets file.
    const std::uint16_t resourceId = scriptPtr.fetchWord(sys->fileEndian());
    /// To be completed ...
    const std::uint8_t freq = scriptPtr.fetchByte();
    /// Volume of the sound.
    const std::uint8_t vol = scriptPtr.fetchByte();
    /// Audio channel in which play the sound.
    const std::uint8_t channel = scriptPtr.fetchByte();
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_playSound(0x%X, %d, %d, %d)",
                              resourceId, freq, vol, channel);
    snd_playSound(resourceId, freq, vol, channel);
}

void AnotherWorld::VirtualMachine::op_updateMemList () {
    /// Identifier of the ressource in the assets file.
    const std::uint16_t resourceId = scriptPtr.fetchWord(sys->fileEndian());
    Logger::getLogger().debug(ILogger::DBG_VM,
                              "VirtualMachine::op_updateMemList(%d)",
                              resourceId);

    if (resourceId == 0) {
        player->stop();
        mixer->stopAll();
        res->invalidateRes();
    } else {
        res->loadPartsOrMemoryEntry(resourceId);
    }
}

void AnotherWorld::VirtualMachine::op_playMusic () {
    /// Identifier of the music in the assets file.
    const std::uint16_t resNum = scriptPtr.fetchWord(sys->fileEndian());
    /// To be completed ...
    const std::uint16_t delay = scriptPtr.fetchWord(sys->fileEndian());
    /// To be completed ...
    const std::uint8_t pos = scriptPtr.fetchByte();
    Logger::getLogger().debug(
        ILogger::DBG_VM, "VirtualMachine::op_playMusic(0x%X, %d, %d)", resNum,
        delay, pos);
    snd_playMusic(resNum, delay, pos);
}

void AnotherWorld::VirtualMachine::initForPart(Game::PartIdentifier partId) {
    player->stop();
    mixer->stopAll();

    // WTF is that ?
    vmVariables[0xE4] = 0x14;

    res->setupPart(partId);

    // Set all thread to inactive (pc at 0xFFFF or 0xFFFE )
    memset((uint8_t *)threadsData, 0xFF, sizeof(threadsData));

    memset((uint8_t *)vmIsChannelActive, 0, sizeof(vmIsChannelActive));

    /// Identifier of the first thread.
    const std::size_t firstThreadId = 0;
    threadsData[PC_OFFSET][firstThreadId] = 0;
}

void AnotherWorld::VirtualMachine::checkThreadRequests () {
    // Check if a part switch has been requested.
    if (res->requestedNextPart != 0) {
        initForPart(res->requestedNextPart);
        res->requestedNextPart = AnotherWorld::Game::INVALID_PART;
    }

    /* Check if a state update has been requested for any thread during the
     * previous VM execution:
     *      - Pause
     *      - Jump
     *
     * JUMP:
     * Note: If a jump has been requested, the jump destination is stored
     * in threadsData[REQUESTED_PC_OFFSET]. Otherwise
     * threadsData[REQUESTED_PC_OFFSET] == 0xFFFF
     *
     * PAUSE:
     * Note: If a pause has been requested it is stored in
     * vmIsChannelActive[REQUESTED_STATE][i] */

    for (std::size_t threadId = 0; threadId < VM_NUM_THREADS; ++threadId) {
        vmIsChannelActive[CURR_STATE][threadId] =
            vmIsChannelActive[REQUESTED_STATE][threadId];

        /// To be completed ...
        const std::uint16_t n = threadsData[REQUESTED_PC_OFFSET][threadId];

        if (n != VM_NO_SETVEC_REQUESTED) {
                if (n == 0xFFFE) {
                    threadsData[PC_OFFSET][threadId] = VM_INACTIVE_THREAD;
                } else {
                    threadsData[PC_OFFSET][threadId] = n;
                }
            threadsData[REQUESTED_PC_OFFSET][threadId] = VM_NO_SETVEC_REQUESTED;
        }
    }
}

void AnotherWorld::VirtualMachine::hostFrame () {
    for (std::size_t threadId = 0; threadId < VM_NUM_THREADS; ++threadId) {
        if (vmIsChannelActive[CURR_STATE][threadId]) continue;

        /// To be completed ...
        const std::uint16_t n = threadsData[PC_OFFSET][threadId];

        if (n != VM_INACTIVE_THREAD) {
            /* Set the script pointer to the right location.
             * script pc is used in executeThread in order
             * to get the next opcode. */
            scriptPtr.pc = res->segBytecode() + n;
            stackPtr = 0;

            gotoNextThread = false;
            Logger::getLogger().debug(ILogger::DBG_VM,
                  "VirtualMachine::hostFrame() i=0x%02X n=0x%02X *p=0x%02X",
                  threadId, n, *scriptPtr.pc);
            executeThread();

            /* Since .pc is going to be modified by this next loop iteration, we
             * need to save it. */
            threadsData[PC_OFFSET][threadId] =
                static_cast<std::uint16_t>(scriptPtr.pc - res->segBytecode());

            Logger::getLogger().debug(
               ILogger::DBG_VM, "VirtualMachine::hostFrame() i=0x%02X pos=0x%X",
                threadId, threadsData[PC_OFFSET][threadId]);
            if (sys->input.quit) {
                break;
            }
        }
    }
}

/// \brief Palette value for black colour.
#define COLOR_BLACK 0xFF
/// \brief Default zoom level.
#define DEFAULT_ZOOM 0x40

void AnotherWorld::VirtualMachine::executeThread () {
    while (!gotoNextThread) {
        /// \brief Identifier for the current opcode.
        const std::uint8_t opcode = scriptPtr.fetchByte();

        // 1000 0000 is set
        if (opcode & 0x80) {
            /// To be completed ...
            const std::uint16_t off = ((opcode << 8) | scriptPtr.fetchByte()) * 2;
            res->setVideoSegmentKind(Resource::USE_CINEMATIC_SEGMENT);
            /// To be completed ...
            std::int16_t x = scriptPtr.fetchByte();
            /// To be completed ...
            std::int16_t y = scriptPtr.fetchByte();
            /// To be completed ...
            const std::int16_t h = y - 199;
            if (h > 0) {
                y = 199;
                x += h;
            }
            Logger::getLogger().debug(
                ILogger::DBG_VIDEO,
                "vid_opcd_0x80 : opcode=0x%X off=0x%X x=%d y=%d", opcode, off,
                x, y);

            // This switch the polygon database to "cinematic" and probably
            // draws a black polygon over all the screen.
            video->setDataBuffer(res->segCinematic(), off);
            video->readAndDrawPolygon(COLOR_BLACK, DEFAULT_ZOOM, Point(x, y));

            continue;
        }

        // 0100 0000 is set
        if (opcode & 0x40) {
            /// To be completed ...
            std::int16_t x;
            /// To be completed ...
            std::int16_t y;
            uint16_t off = scriptPtr.fetchWord(sys->fileEndian()) * 2;
            x = scriptPtr.fetchByte();

            res->setVideoSegmentKind(Resource::USE_CINEMATIC_SEGMENT);

            if (!(opcode & 0x20)) {
                if (!(opcode & 0x10))  // 0001 0000 is set
                {
                    x = (x << 8) | scriptPtr.fetchByte();
                } else {
                    x = vmVariables[x];
                }
            } else {
                if (opcode & 0x10) {  // 0001 0000 is set
                    x += 0x100;
                }
            }

            y = scriptPtr.fetchByte();

            if (!(opcode & 8)) {  // 0000 1000 is set
                if (!(opcode & 4)) {  // 0000 0100 is set
                    y = (y << 8) | scriptPtr.fetchByte();
                } else {
                    y = vmVariables[y];
                }
            }

            /// To be completed ...
            std::uint16_t zoom = scriptPtr.fetchByte();

            if (!(opcode & 2)) {  // 0000 0010 is set
                if (!(opcode & 1)) {  // 0000 0001 is set
                    --scriptPtr.pc;
                    zoom = 0x40;
                } else {
                    zoom = vmVariables[zoom];
                }
            } else {
                if (opcode & 1) {  // 0000 0001 is set
                    res->setVideoSegmentKind(Resource::USE_VIDEO_SEGMENT);
                    --scriptPtr.pc;
                    zoom = 0x40;
                }
            }
            Logger::getLogger().debug(ILogger::DBG_VIDEO,
                                      "vid_opcd_0x40 : off=0x%X x=%d y=%d",
                                      off, x, y);
            video->setDataBuffer(res->getVideoBuffer(), off);

            video->readAndDrawPolygon(0xFF, zoom, Point(x, y));

            continue;
        }

        if (opcode > 0x1A) {
            Logger::getLogger().error(
                "VirtualMachine::executeThread() ec=0x%X invalid opcode=0x%X",
                0xFFF, opcode);
        } else {
            (this->*opcodeTable[opcode])();
        }
    }
}

void AnotherWorld::VirtualMachine::inp_updatePlayer () {
    sys->processEvents();

    /// Identifier of the current game part.
    const auto currentPartId = res->currentPartId();
    if (currentPartId == Game::PART9 || currentPartId == Game::PART10) {
        char c = sys->input.lastChar;
        if (c == 8 || /*c == 0xD |*/ c == 0 || (c >= 'a' && c <= 'z')) {
            vmVariables[VM_VARIABLE_LAST_KEYCHAR] =
                static_cast<int16_t>(c & ~0x20);
            sys->input.lastChar = 0;
        }
    }

    /// To be completed ...
    std::int16_t lr = 0;
    /// To be completed ...
    std::int16_t m = 0;
    /// To be completed ...
    std::int16_t ud = 0;

    if (sys->input.dirMask & PlayerInput::DIR_RIGHT) {
        lr = 1;
        m |= 1;
    }
    if (sys->input.dirMask & PlayerInput::DIR_LEFT) {
        lr = -1;
        m |= 2;
    }
    if (sys->input.dirMask & PlayerInput::DIR_DOWN) {
        ud = 1;
        m |= 4;
    }

    vmVariables[VM_VARIABLE_HERO_POS_UP_DOWN] = ud;

    if (sys->input.dirMask & PlayerInput::DIR_UP) {
        vmVariables[VM_VARIABLE_HERO_POS_UP_DOWN] = -1;
    }

    if (sys->input.dirMask & PlayerInput::DIR_UP) {  // inpJump
        ud = -1;
        m |= 8;
    }

    vmVariables[VM_VARIABLE_HERO_POS_JUMP_DOWN] = ud;
    vmVariables[VM_VARIABLE_HERO_POS_LEFT_RIGHT] = lr;
    vmVariables[VM_VARIABLE_HERO_POS_MASK] = m;
    /// Whether or not the action button has been pressed.
    std::int16_t button = 0;

    if (sys->input.button) {  // inpButton
        button = 1;
        m |= 0x80;
    }

    vmVariables[VM_VARIABLE_HERO_ACTION] = button;
    vmVariables[VM_VARIABLE_HERO_ACTION_POS_MASK] = m;
}

void AnotherWorld::VirtualMachine::inp_handleSpecialKeys() {
    if (sys->input.pause) {
        if (res->currentPartId() != Game::PART1 &&
            res->currentPartId() != Game::PART2) {
            sys->input.pause = false;
            while (!sys->input.pause) {
                sys->processEvents();
                sys->sleep(200);
            }
        }
        sys->input.pause = false;
    }

    if (sys->input.code) {
        sys->input.code = false;
        if (res->currentPartId() != Game::PART_LAST &&
            res->currentPartId() != Game::PART_FIRST) {
            res->requestedNextPart = Game::PART_LAST;
        }
    }

    // XXX
    if (vmVariables[0xC9] == 1) {
        Logger::getLogger().warning(
            "VirtualMachine::inp_handleSpecialKeys() unhandled case "
            "(vmVariables[0xC9] == 1)");
    }
}

void AnotherWorld::VirtualMachine::snd_playSound (std::uint16_t resNum,
                                                  std::uint8_t freq,
                                                  std::uint8_t vol,
                                                  std::uint8_t channel) {
    Logger::getLogger().debug(ILogger::DBG_SND,
                              "snd_playSound(0x%X, %d, %d, %d)", resNum, freq,
                              vol, channel);

    /// Pointer to access game assets.
    MemEntry* me = &res->memList[resNum];

    if (me->state != MemState::STATE_LOADED) return;

    if (vol == 0) {
        mixer->stopChannel(channel);
    } else {
        /// Mixer for game sounds.
        MixerChunk mc;
        mc.data = me->bufPtr + 8;  // skip header
        mc.len = file2Native(*reinterpret_cast<std::uint16_t*>(me->bufPtr),
                             sys->fileEndian()) * 2;
        mc.loopLen = file2Native(*reinterpret_cast<std::uint16_t*>(me->bufPtr
                                                                   + 2),
                                 sys->fileEndian()) * 2;
        if (mc.loopLen != 0) {
            mc.loopPos = mc.len;
        }
        assert(freq < 40);
        mixer->playChannel(channel & 3, mc, frequenceTable[freq],
                           std::min(vol, static_cast<uint8_t>(0x3F)));
    }
}

void AnotherWorld::VirtualMachine::snd_playMusic(std::uint16_t resNum,
                                                 std::uint16_t delay,
                                                 std::uint8_t pos) {
    Logger::getLogger().debug(ILogger::DBG_SND, "snd_playMusic(0x%X, %d, %d)",
                              resNum, delay, pos);

    if (resNum != 0) {
        player->loadSfxModule(resNum, delay, pos);
        player->start();
    } else if (delay != 0) {
        player->setEventsDelay(delay);
    } else {
        player->stop();
    }
}
