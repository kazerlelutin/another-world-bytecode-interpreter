/**
 * \file engine.cpp
 * \brief Game engine implementation.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Glaize, Sylvain
 */

#include "engine.hpp"

#include <iostream>

#include "errors.hpp"
#include "endian.hpp"
#include "titleDetector.hpp"

AnotherWorld::Engine::Engine (SDLStub* paramSys, const std::string& _dataDir,
                              uint8_t volume):
        dataDir (_dataDir), sys (paramSys), mixer (sys, volume),
        res (&video, dataDir.c_str()), video (&res, sys),
        player (&mixer, &res, sys), detection (dataDir),
        vm (&mixer, &res, &player, &video, sys, detection.getDataType()) {
    logsTitleDetection(detection);

    sys->init(detection.getGameTitle(LANG_FR), bigEndian);
    res.readEntries(detection);

    /**
     * At which part of the game to start. GAME_PART1 leads to
     * protection screen, while GAME_PART2 leads directly to game
     * introduction, bypassing the protection. To jump to further
     * part of the game, you also must set the game state,
     * otherwise it will crash.
     */
#ifdef BYPASS_PROTECTION
    Game::PartIdentifier part = Game::PART2;
#else
    Game::PartIdentifier part = Game::PART1;
#endif
    vm.initForPart(part);
}

int AnotherWorld::Engine::run () {
    try {
        while (!sys->input.quit) {
            vm.checkThreadRequests();

            vm.inp_updatePlayer();

            vm.hostFrame();
        }

        return 0;
    }
    catch (MemoryReadingError &e) {
        std::cerr << e.what() << '\n';
        return -8;
    }
    catch (UnrecoverableError &e) {
        std::cerr << e.what() << '\n';
        return -12;
    }
}
