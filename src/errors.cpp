/**
 * \file errors.cpp
 * \brief Implementation of errors handling.
 * \author Le Bars, Yoann
 */

#include "errors.hpp"

#include <sstream>
#include <boost/filesystem.hpp>

const char* AnotherWorld::UnlocatableFile::what () const throw () {
    /// Error message.
    static std::string errMess;
    /// Stream on the error message.
    std::stringstream sStream (errMess);
    sStream << "Unable to locate game data file named \"" << fileName_
            << "\".";
    return errMess.c_str();
}

const char* AnotherWorld::MemoryReadingError::what() const throw () {
    /// Error message.
    static std::string errMess;
    /// Stream on the error message.
    std::stringstream sStream (errMess);
    /// Path to the file.
    const boost::filesystem::path path (filePath_);
    /// Name of the file.
    const std::string fileName = path.filename().string();
    sStream << "An error occurred while reading memory list entries in file \""
            << fileName << "\".";
    return errMess.c_str();
}

const char* AnotherWorld::FailedSignatureReading::what () const throw () {
    /// Error message.
    static std::string errMess;
    /// Stream on the error message.
    std::stringstream sStream (errMess);
    sStream << "An error occurred when looking for game signature in the "
            << "file named \"" << fileName_ << "\".";
    return errMess.c_str();
}

const char* AnotherWorld::NoValidSignature::what() const throw () {
    /// Error message.
    static std::string errMess;
    /// Stream on the error message.
    std::stringstream sStream (errMess);
    sStream << "Cannot locate valid game signature in file named \""
            << fileName_ << "\".";
    return errMess.c_str();
}
