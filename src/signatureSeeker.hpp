#ifndef __SIGNATURESEEKER_HPP__
#define __SIGNATURESEEKER_HPP__

/**
 * \file signatureSeeker.hpp
 * \brief Searches for signature in a file.
 * \author Glaize, Sylvain
 * \version 1.0
 */

#include "titleDetector.hpp"

#include <memory>

namespace AnotherWorld {
    class File;

    class SignatureSeeker {
       public:
        /**
         * \brief The default destructor needed for usage of unique_ptr
         */
        virtual ~SignatureSeeker () = default;

        /**
         * \brief Places the file at the beginning of the signature.
         * \param inputFile The file to searches the signature in.
         */
        virtual void seek (File& inputFile) const = 0;

        /**
         * \brief Gets the filename to search the signature in.
         * \return The filename to search the signature in.
         */
        [[nodiscard]] virtual const std::string &getFilename () const = 0;
    };

    /**
     * \brief Gets an instance of a signature seeker corresponding of the data type.
     * \param dataType The data type associated to the game data to read.
     * \return The instance of the signature seeker.
     * \throws UnsupportedDataType
     */
    std::unique_ptr<SignatureSeeker> getSignatureSeeker (DataType dataType);
}

#endif
