/**
 * \file resourceStats.cpp
 * \brief Implementation of statistics handling of resources.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Glaize, Sylvain
 */

#include "resourceStats.hpp"

#include "log.hpp"

namespace AnotherWorld {
    /// \brief Local utilitarian functions.
    namespace Private {
        /**
         * \brief Compute a percentage from two values.
         * \param biggest Biggest value for the statistics.
         * \param lowest lowest value for the statistics.
         */
        template <typename T>
        float ratio_percentage (T biggest, T lowest) {
            return 100.f * (static_cast<float>(biggest)
                - static_cast<float>(lowest)) / static_cast<float>(biggest);
        }

        /**
         * \brief Compute the percentage of a value compare to the total.
         * \param figure Value to test.
         * \param total Total of resources.
         */
        template <typename T, typename U>
        float percentage_of_total (T figure, U total) {
            return 100.f * static_cast<float>(figure)
                / static_cast<float>(total);
        }
    }

    void ResourceStats::addEntry(const MemEntry &memEntry) {
        logEntry(memEntry);

        if (memEntry.packedSize == memEntry.size) {
            ++resourceUnitStats[memEntry.type][UNCOMPRESSED_INDEX];
            ++resourceUnitStats[TOTAL_INDEX][UNCOMPRESSED_INDEX];
        } else {
            ++resourceUnitStats[memEntry.type][COMPRESSED_INDEX];
            ++resourceUnitStats[TOTAL_INDEX][COMPRESSED_INDEX];
        }

        resourceSizeStats[memEntry.type][UNCOMPRESSED_INDEX] += memEntry.size;
        resourceSizeStats[TOTAL_INDEX][UNCOMPRESSED_INDEX] += memEntry.size;
        resourceSizeStats[memEntry.type][COMPRESSED_INDEX] +=
            memEntry.packedSize;
        resourceSizeStats[TOTAL_INDEX][COMPRESSED_INDEX] += memEntry.packedSize;

        resourceCount += 1;
    }

    void ResourceStats::logSummary() {
        /// Logger describer for statistics.
        auto &logger = Logger::getLogger();

        /// Size of data when compressed.
        const auto totalCompressedCount =
            resourceUnitStats[TOTAL_INDEX][COMPRESSED_INDEX];
        /// Size of data when uncompressed.
        const auto totalUncompressedCount =
            resourceUnitStats[TOTAL_INDEX][UNCOMPRESSED_INDEX];
        logger.debug(ILogger::DBG_RES, "\n");
        logger.debug(ILogger::DBG_RES, "Total # resources: %d", resourceCount);
        logger.debug(ILogger::DBG_RES, "Compressed       : %d",
                     totalCompressedCount);
        logger.debug(ILogger::DBG_RES, "Uncompressed     : %d",
                     totalUncompressedCount);
        logger.debug(
            ILogger::DBG_RES, "Note: %2.0f%% of resources are compressed.",
            Private::percentage_of_total(totalCompressedCount,
                                         resourceCount));

        /// Size of data when compressed.
        const auto totalCompressedSize =
            resourceSizeStats[TOTAL_INDEX][COMPRESSED_INDEX];
        /// Size of data when uncompressed.
        const auto totalUncompressedSize =
            resourceSizeStats[TOTAL_INDEX][UNCOMPRESSED_INDEX];

        logger.debug(ILogger::DBG_RES, "\n");
        logger.debug(ILogger::DBG_RES, "Total size (compressed)   : %7d bytes.",
                     totalCompressedSize);
        logger.debug(ILogger::DBG_RES, "Total size (uncompressed) : %7d bytes.",
                     totalUncompressedSize);
        logger.debug(
            ILogger::DBG_RES, "Note: Overall compression gain is : %2.0f%%.",
            Private::ratio_percentage(totalUncompressedSize,
                                      totalCompressedSize));

        logger.debug(ILogger::DBG_RES, "\n");

        for (int resourceTypeIndex = 0; resourceTypeIndex < STAT_ARRAY_SIZE - 1;
             resourceTypeIndex++) {
            /// Size of data when compressed.
            const auto resourceCompressedSize =
                resourceSizeStats[resourceTypeIndex][COMPRESSED_INDEX];
            /// Size of data when uncompressed.
            const auto resourceUncompressedSize =
                resourceSizeStats[resourceTypeIndex][UNCOMPRESSED_INDEX];

            logger.debug(
                ILogger::DBG_RES,
                "Total %-17s unpacked size: %7d (%2.0f%% of total unpacked "
                "size) "
                "packedSize %7d (%2.0f%% of floppy space) gain:(%2.0f%%)",
                resTypeToString(resourceTypeIndex), resourceUncompressedSize,
                Private::percentage_of_total(resourceUncompressedSize,
                                             totalUncompressedSize),
                resourceCompressedSize,
                Private::percentage_of_total(resourceCompressedSize,
                                             totalCompressedSize),
                Private::ratio_percentage(resourceUncompressedSize,
                                          resourceCompressedSize));
            }

            logger.debug(ILogger::DBG_RES,
                         "Note: Damn you sound compression rate!");
            logger.debug(ILogger::DBG_RES, "\n");
            logger.debug(ILogger::DBG_RES, "Total bank files:              %d",
                         totalUncompressedCount + totalCompressedCount);

            for (int resourceTypeIndex = 0;
                 resourceTypeIndex < STAT_ARRAY_SIZE - 1;
                 resourceTypeIndex++) {
                logger.debug(
                    ILogger::DBG_RES, "Total %-17s files: %3d",
                    resTypeToString(resourceTypeIndex),
                    resourceUnitStats[resourceTypeIndex][UNCOMPRESSED_INDEX] +
                    resourceUnitStats[resourceTypeIndex][COMPRESSED_INDEX]);
            }
        }

    void ResourceStats::logEntry (const MemEntry& memEntry) const {
        /// Size of the data entry when unpacked.
        const auto size = memEntry.size;
        /// Size of the data entry when packed.
        const auto packedSize = memEntry.packedSize;
        /// Memory space gained.
        const auto gain = size ? static_cast<float>(size - packedSize) /
                                    static_cast<float>(size) * 100.0f
                               : 0.0f;

        Logger::getLogger().debug(
            ILogger::DBG_RES,
            "R:0x%02X,%-17s size=%5d (compacted gain=%2.0f%%)",
            resourceCount, resTypeToString(memEntry.type), size, gain);
    }
}
