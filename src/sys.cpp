/**
 * \file sys.cpp
 * \brief Definition of the system running the game.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Glaize, Sylvain
 */

#include "sys.hpp"

#include <cstring>
#include <cstdint>
#include <cstdlib>
#include <boost/filesystem.hpp>

#include <iostream>

#include "log.hpp"

#include "controls/keyboard.hpp"
#include "controls/joystick.hpp"

namespace AnotherWorld {
    namespace fs = boost::filesystem;

    /// \brief Game colour palette.
    static SDL_Color palette [ColorsMaxRender];

    SDLStub::~SDLStub () {
        cleanupGfxMode();

        for (auto controller: controllers) {
            controller.second->close();
            delete controller.second;
            controller.second = nullptr;
        }

        SDL_Quit();
    }

    void SDLStub::init (const std::string& _gameTitle, Endian _fileEndian) {
        /// Sets the game title
        gameTitle = _gameTitle;
        /// Flags to set up the system.
        std::uint32_t SDLFlags = SDL_INIT_VIDEO | SDL_INIT_AUDIO
                                 | SDL_INIT_TIMER | SDL_INIT_JOYSTICK
                                 | SDL_INIT_GAMECONTROLLER;

        /// Error code.
        const int error = SDL_Init(SDLFlags);

        if (error < 0) {
            // FIXME: an exception caught in main() should be raised at this  point
            Logger::getLogger().error(
                    "SDLStub::init() enable to init SDL %s", SDL_GetError());
        }

        // SDL_EnableKeyRepeat(SDL_DEFAULT_REPEAT_DELAY,
        // SDL_DEFAULT_REPEAT_INTERVAL);
        SDL_ShowCursor(SDL_DISABLE);

        SDL_CaptureMouse(SDL_TRUE);
        
        SDL_JoystickEventState(SDL_ENABLE);
        SDL_GameControllerEventState(SDL_ENABLE);

        /* We always need the keyboard to play! */
        controllers.insert({Control::KEYBOARD, new Control::Keyboard()});

        /* Only one joystick available in this game. */
        controllers.insert({Control::JOYSTICK, new Control::Joystick()});


        /// Path to the executable.
        const fs::path _executablePath =
                boost::filesystem::system_complete(executablePath);
        /// Path to icon file.
        fs::path gamecontrollerdb = _executablePath.parent_path();
        gamecontrollerdb /= "gamecontrollerdb.txt";

        int nummappings = SDL_GameControllerAddMappingsFromFile(gamecontrollerdb.string().c_str());

        if (nummappings > 0) {
            Logger::getLogger().information(
                    "SDLStub::init() %d mappings loaded from gamecontrollerdb.txt", nummappings);
        } else {
            Logger::getLogger().warning(
                    "SDLStub::init() could not load mappings gamecontrollerdb.txt : %s", SDL_GetError());
        }

        if (SDL_NumJoysticks() > 0) {
            Logger::getLogger().information(
                    "SDLStub::init() %d joysticks detected", SDL_NumJoysticks());

            controllers[Control::JOYSTICK]->open(0);
        } else {
            Logger::getLogger().information(
                    "SDLStub::init() no joystick detected");
        }

        std::memset(&input, 0, sizeof(input));
        scale = DEFAULT_SCALE;
        prepareGfxMode();

        fileEndian_ = _fileEndian;
    }

    void SDLStub::setPalette (const std::uint8_t* paletteBuffer) {
        // The incoming palette is in 565 format.
        for (auto &i : palette) {
            /// To be completed ...
            const std::uint8_t c1 = *(paletteBuffer + 0);
            /// To be completed ...
            const std::uint8_t c2 = *(paletteBuffer + 1);
            i.r = (((c1 & 0x0F) << 2) | ((c1 & 0x0F) >> 2)) << 2;  // r
            i.g = (((c2 & 0xF0) >> 2) | ((c2 & 0xF0) >> 6)) << 2;  // g
            i.b = (((c2 & 0x0F) >> 2) | ((c2 & 0x0F) << 2)) << 2;  // b
            i.a = 0xFF;
            paletteBuffer += 2;
        }

        if(SDL_SetPaletteColors(screen->format->palette, palette, 0, ColorsMaxRender)) {
            Logger::getLogger().warning("Problem while setting palette colors : %s", SDL_GetError());
        }
    }

    void SDLStub::prepareGfxMode() {
        /// Screen width.
        const int w = SCREEN_W;
        /// Screen height.
        const int h = SCREEN_H;

        SDL_WindowFlags flag = SDL_WINDOW_SHOWN;

        if (fullscreen) {
            flag = SDL_WINDOW_FULLSCREEN;
        }

        window = SDL_CreateWindow(gameTitle.c_str(), SDL_WINDOWPOS_CENTERED,
                                  SDL_WINDOWPOS_CENTERED, w * scale, h * scale,
                                  flag);

        if (!window) {
            Logger::getLogger().error("Cannot create window : %s",
                                      SDL_GetError());
        }

        /// Path to the executable.
        const fs::path _executablePath =
            boost::filesystem::system_complete(executablePath);
        /// Path to icon file.
        fs::path iconPath = _executablePath.parent_path();
        iconPath /= "logo-aw.bmp";
        if (fs::exists(iconPath)) {
            icon = SDL_LoadBMP(iconPath.string().c_str());
            if (icon == nullptr) {
                Logger::getLogger().warning("Error when loading game icon: %s",
                                            SDL_GetError());
            } else {
                SDL_SetWindowIcon(window, icon);
            }
        } else {
            Logger::getLogger().warning("Game icon not found.\n");
        }

        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED
                                                  | SDL_RENDERER_PRESENTVSYNC);
        if (renderer == nullptr) {
            Logger::getLogger().error("Cannot create renderer: %s",
                                      SDL_GetError());
        }

        screen = SDL_CreateRGBSurface(SDL_SWSURFACE, w, h, 8, 0, 0, 0, 0);
        if (screen == nullptr) {
            Logger::getLogger().error("Unable to allocate screen buffer:  %s",
                                      SDL_GetError());
        }
        /* Upon resize during gameplay, the screen surface is re-created and a
         * new palette is allocated. This will result in an all-white surface
         * palette displaying a window full of white until a a palette is set
         * by the VM. To avoid this issue, we save the last palette locally and
         * re-upload it each time. On game start-up this is not requested. */
        if(SDL_SetPaletteColors(screen->format->palette, palette, 0,
                                ColorsMaxRender)) {
            Logger::getLogger().warning(
                "Problem while setting palette colors while resizing : %s",
                SDL_GetError());
        }
    }

    void SDLStub::updateDisplay (const std::uint8_t *src) {
        std::uint16_t height = SCREEN_H;
        /// Pixels array.
        auto* pixels = static_cast<std::uint8_t*>(screen->pixels);

        // For each line
        while (height--) {
            /* One byte gives us two pixels, we only need to iterate w / 2
             * times. */
            for (int i = 0; i < SCREEN_W / 2; ++i) {
                /* Extract two palette indices from upper byte and lower
                 *byte. */
                pixels[i * 2 + 0] = *(src + i) >> 4;
                pixels[i * 2 + 1] = *(src + i) & 0xF;
            }
            pixels += screen->pitch;
            src += SCREEN_W / 2;
        }

        SDL_Texture* texture = SDL_CreateTextureFromSurface(renderer, screen);

        if (texture == nullptr) {
            Logger::getLogger().warning("Cannot create texture : %s", SDL_GetError());
        }

        if (SDL_RenderCopy(renderer, texture, nullptr, nullptr)) {
            Logger::getLogger().warning("Cannot render surface : %s", SDL_GetError());
        }
        SDL_RenderPresent(renderer);
        if (texture != nullptr) {
            SDL_DestroyTexture(texture);
            texture = nullptr;
        }
    }

    void SDLStub::processEvents () {
        /// Event describer.
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            for (auto controller: controllers) {
                if(controller.second->processEvents(event, *this)) {
                    break;
                }
            }
        }
    }

    void SDLStub::startAudio (AudioCallback callback, void* param) {
        /// Asked audio specifications.
        SDL_AudioSpec desired;
        std::memset(&desired, 0, sizeof(desired));

        desired.freq = SOUND_SAMPLE_RATE;
        desired.format = AUDIO_U8;
        desired.channels = 1;
        desired.samples = 2048;
        desired.callback = callback;
        desired.userdata = param;
        if (SDL_OpenAudio(&desired, nullptr) == 0) {
            SDL_PauseAudio(0);
        } else {
            Logger::getLogger().error(
                    "SDLStub::startAudio() unable to open sound device : %s", SDL_GetError());
        }
    }

    void SDLStub::cleanupGfxMode () {
        if (renderer != nullptr) {
            SDL_DestroyRenderer(renderer);
            renderer = nullptr;
        }

        if (screen != nullptr) {
            SDL_FreeSurface(screen);
            screen = nullptr;
         }

        if (icon != nullptr) {
            SDL_FreeSurface(icon);
            icon = nullptr;
        }

        if (window != nullptr) {
            SDL_DestroyWindow(window);
            window = nullptr;
        }
    }

    void SDLStub::switchGfxMode () {
        cleanupGfxMode();
        prepareGfxMode();
    }
}
