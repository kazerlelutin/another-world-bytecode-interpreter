/**
 * \file memListReader.cpp
 * \brief Reads the resource directory.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author willll
 * \author Glaize, Sylvain
 */

#include "memListReader.hpp"

#include <iostream>

#include "errors.hpp"
#include "file.hpp"
#include "log.hpp"
#include "parts.hpp"
#include "resourceStats.hpp"
#include "video.hpp"
#include "signatureSeeker.hpp"

namespace AnotherWorld {
    /**
     * \brief Set entries reader.
     * \param inputFile File containing game assets.
     * \param memList Pointer to where stock data in memory.
     * \returns Number of entry read.
     * \throws MemoryReadingError
     */
    std::int16_t readMemList (File &inputFile, MemEntry* memList) {
        /// Statistics container.
        ResourceStats resourceStats;

        /// Data localisation in memory.
        auto memEntry = memList;
        /// Asset identifier.
        std::int16_t resourceCount = 0;
        while (true) {
            /// Current entry being loaded.
            memEntry->state = inputFile.read<uint8_t>();
            memEntry->type = inputFile.read<uint8_t>();
            memEntry->bufPtr = nullptr;
            inputFile.read<uint32_t>();
            memEntry->rankNum = inputFile.read<uint8_t>();
            memEntry->bankId = inputFile.read<uint8_t>();
            memEntry->bankOffset = inputFile.read<uint32_t>();
            memEntry->packedSize = inputFile.read<uint32_t>();
            memEntry->size = inputFile.read<uint32_t>();
            memEntry->fileEndianness = inputFile.endianness();

            if (!inputFile.good()) {
                /// Error message.
                const char* message =
                    "An error occurred while reading the memory list "
                    "entries.\n";
                Logger::getLogger().error(message);
                throw MemoryReadingError (inputFile.filePath());
            }

            if (memEntry->state == MemState::STATE_END_OF_MEMLIST ||
                memEntry->size == std::numeric_limits<uint32_t>::max()) {
                break;
            }

            // Memory tracking
            resourceStats.addEntry(*memEntry);

            ++resourceCount;
            ++memEntry;
        }

        resourceStats.logSummary();
        return resourceCount;
    }

    std::int16_t readEntriesFromFile (const std::string &directory,
                                      MemEntry* memList,
                                      const SignatureSeeker &seeker) {
        /// Describer of the file to be read.
        File f;
        /// Name of the file to open.
        const std::string fileName = seeker.getFilename();

        if (!openFile(f, fileName, directory, bigEndian)) {
            Logger::getLogger().error(
                "Resource::readEntries() unable to open '%s' file\n",
                fileName.c_str());
            throw UnlocatableFile (fileName);
        }

        seeker.seek(f);

        std::int16_t resourceCount = readMemList(f, memList);
        return resourceCount;
    }
}
