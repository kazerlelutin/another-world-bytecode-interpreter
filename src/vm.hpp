#ifndef __VM_HPP__
#define __VM_HPP__

/**
 * \file vm.hpp
 * \brief Definition for the virtual machine of the game.
 * \author Montoir, Gregory
 * \author Sanglard, Fabien
 * \author Le Bars, Yoann
 * \author Glaize, Sylvain
 */

#include <cstdint>

#include "intern.hpp"
#include "parts.hpp"
#include "titleDetector.hpp"

/// \brief Number of thread the virtual machine can run.
const std::size_t VM_NUM_THREADS = 64;

/// \brief Number of registers available in the virtual machine.
const std::size_t VM_NUM_VARIABLES = 256;

/// \brief To be completed ...
#define VM_NO_SETVEC_REQUESTED 0xFFFF

/// \brief Identifier indicating a thread is not activated.
#define VM_INACTIVE_THREAD 0xFFFF

/// \brief For threadsData navigation
#define PC_OFFSET 0

/// \brief To be completed ...
#define REQUESTED_PC_OFFSET 1

/// \brief For vmIsChannelActive navigation
#define CURR_STATE 0

/// \brief To be completed ...
#define REQUESTED_STATE 1

namespace AnotherWorld {
    // Forward declarations.
    class Mixer;
    class Resource;
    class SfxPlayer;
    class SDLStub;
    class Video;

    /// \brief Reference variables of the virtual machine.
    enum ScriptVars {
        /**
         * \brief Identifier of the register containing the seed for random numbers
         * generation.
         */
        VM_VARIABLE_RANDOM_SEED = 0x3C,

        /// \brief To be completed ...
        VM_VARIABLE_LAST_KEYCHAR = 0xDA,

        /**
         * \brief Identifier of the register indicating if the character is going
         * up or down (used in swimming parts).
         */
        VM_VARIABLE_HERO_POS_UP_DOWN = 0xE5,

        /// \brief To be completed ...
        VM_VARIABLE_MUS_MARK = 0xF4,

        /**
         * \brief Identifier of the register indicating the where is the end of
         * the vertical value of the screen, to implement scrolling.
         */
        VM_VARIABLE_SCROLL_Y = 0xF9,  // = 239

        /**
         * \brief Identifier of the register indicating if the action button is
         * pressed.
         */
        VM_VARIABLE_HERO_ACTION = 0xFA,

        /**
         * \brief Identifier of the register indicating if the character should
         * jump or crouch down.
         */
        VM_VARIABLE_HERO_POS_JUMP_DOWN = 0xFB,

        /// \brief To be completed ...
        VM_VARIABLE_HERO_POS_LEFT_RIGHT = 0xFC,

        /// \brief To be completed ...
        VM_VARIABLE_HERO_POS_MASK = 0xFD,

        /// \brief To be completed ...
        VM_VARIABLE_HERO_ACTION_POS_MASK = 0xFE,

        /// \brief To be completed ...
        VM_VARIABLE_PAUSE_SLICES = 0xFF
    };

    /// \brief How many data fields for a thread.
    const std::size_t NUM_DATA_FIELDS = 2;

    /// \brief To be completed ...
    const std::size_t NUM_THREAD_FIELDS = 2;

    /// \brief Class to execute game opcodes.
    class VirtualMachine {
        public:
            /**
             * \brief The type of entries in opcodeTable. This allows "fast"
             * branching.
             */
            typedef void (VirtualMachine::*OpcodeStub) ();

            /**
             * \brief Constructor.
             * \param mix Pointer to game mixer.
             * \param _res Pointer to access game resources.
             * \param ply Pointer to game sound effects player.
             * \param vid Pointer to access video screen game.
             * \param stub System describer.
             */
            VirtualMachine(Mixer* mix, Resource* _res, SfxPlayer* ply,
                           Video* vid, SDLStub* stub,
                           AnotherWorld::DataType type);

            /**
             * \brief Initiates a part of the game.
             * \param partId Identifier of the part to be set up.
             */
            void initForPart (Game::PartIdentifier partId);

            /**
             * \brief To be completed ...
             *
             * This is called every frames in the infinite loop.
             */
            void checkThreadRequests ();

            /**
             * \brief Run the Virtual Machine for every active threads (one vm
             * frame).
             *
             * Inactive threads are marked with a thread instruction pointer
             * set to 0xFFFF (VM_INACTIVE_THREAD). A thread must feature a
             * break opcode so the interpreter can move to the next thread.
             */
            void hostFrame ();

            /// \brief To be completed ...
            void executeThread ();

            /// \brief To be completed ...
            void inp_updatePlayer ();

            /// \brief To be completed ...
            void inp_handleSpecialKeys ();

            /**
             * \brief Play a sound.
             * \param resNum To be completed ...
             * \param freq Sound sampling frequency.
             * \param vol Sound volume.
             * \param channel Audio channel in which play the sound.
             */
            void snd_playSound (std::uint16_t resNum, std::uint8_t freq,
                                std::uint8_t vol, std::uint8_t channel);

            /**
             * \brief Play a music.
             * \param resNum To be completed ...
             * \param delay To be completed ...
             * \param pos To be completed ...
             */
            void snd_playMusic (std::uint16_t resNum, std::uint16_t delay,
                                std::uint8_t pos);

        protected:
            /// \brief Opcode to move a constante into a register.
            void op_movConst ();

            /// \brief Opcode to copy the value of a register to another.
            void op_mov ();

            /// \brief Opcode to add the value of a register to another.
            void op_add ();

            /// \brief Opcode to add a constant to a register.
            void op_addConst ();

            /// \brief To be completed ...
            void op_call ();

            /// \brief To be completed ...
            void op_ret ();

            /// \brief To be completed ...
            void op_pauseThread ();

            /// \brief Opcode to jump to another location into the code.
            void op_jmp ();

            /// \brief To be completed ...
            void op_setSetVect ();

            /**
             * \brief Opcode to jump to a location in the code if the value in
             * the given register, after being decreased, is not zero.
             */
            void op_jnz ();

            /**
             * \brief Opcode to compare values into two registers and jump into
             * some part of the code depending on the result of the comparison.
             */
            void op_condJmp ();

            /// \brief Opcode to change colour palette.
            void op_setPalette ();

            /// \brief To be completed ...
            void op_resetThread();

            /// \brief Opcode to switch from one framebuffer to another.
            void op_selectVideoPage ();

            /// \brief Opcode to fill in a framebuffer with a given colour.
            void op_fillVideoPage ();

            /**
             * \brief Opcode that Copies content from one framebuffer to
             * another.
             */
            void op_copyVideoPage ();

            /// \brief To be completed ...
            void op_blitFramebuffer ();

            /// \brief To be completed ...
            void op_killThread ();

            /// \brief Opcode that writes down a string.
            void op_drawString ();

            /// \brief Opcode to subtract a value from a register to another.
            void op_sub ();

            /**
             * \brief Opcode that execute a logical and to a given register
             * from a given value.
             */
            void op_and ();

            /**
             * \brief Opcode that execute a logical or to a given register from
             * a given value.
             */
            void op_or ();

            /**
             * \brief Opcode that shift to the left the value of a given
             * register.
             */
            void op_shl ();

            /**
             * \brief Opcode that shift to the right the value of a given
             * register.
             */
            void op_shr ();

            /// \brief Opcode to play a sound effect.
            void op_playSound ();

            /// \brief To be completed ...
            void op_updateMemList ();

            /// \brief Opcode to play a music.
            void op_playMusic ();

        private:
            /// \brief Opcodes descriptor.
            static const OpcodeStub opcodeTable [];

            /// \brief This table is used to play a sound.
            static const std::uint16_t frequenceTable [];

            /// \brief Pointer to access to game sound mixer.
            Mixer* mixer;

            /// \brief Pointer to access to game resources.
            Resource* res;

            /// \brief Pointer to access game sound effects player.
            SfxPlayer* player;

            /// \brief Pointer to access video ressources.
            Video* video;

            /// \brief System describer.
            SDLStub* sys;

            /// \brief Set of registers of the virtual machine.
            std::int16_t vmVariables [VM_NUM_VARIABLES] {};

            /// \brief Stack of scripts to be called.
            std::uint16_t scriptStackCalls [VM_NUM_THREADS] {};

            /// \brief Data for each thread.
            std::uint16_t threadsData [NUM_DATA_FIELDS][VM_NUM_THREADS] {};

            /**
             * \brief Indicating whether or not a channel of the virtual
             * machine is active.
             *
             * This array is used:
             *     0 to save the channel's instruction pointer
             *     when the channel release control (this happens on a break).
             *     1 When a setVec is requested for the next vm frame.
             */
            std::uint8_t vmIsChannelActive [NUM_THREAD_FIELDS][VM_NUM_THREADS] {};

            /// \brief Pointer to access game opcodes.
            Ptr scriptPtr;

            /// \brief To be completed ...
            std::uint8_t stackPtr;

            /// \brief Whether or not change thread.
            bool gotoNextThread;

            /// \brief To be completed ...
            std::uint32_t lastTimeStamp;
    };
}

#endif
